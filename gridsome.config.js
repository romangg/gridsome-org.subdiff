// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`
const marked = require('marked')

function sluggify_to_url(text) {
  // 1. Convert text to lower case.
  // 2. Replace one or multiple hyphens with a single space.
  // 3. Remove everything which is not alphanummeric or space.
  // 4. Replace one or multiple spaces with a single hyphen.
  return text.toLowerCase().replace(/-+/g, ' ').replace(/[^\w ]+/g, '').replace(/ +/g, '-');
}

module.exports = {
  siteName: 'subdiff.org',
  siteUrl: 'https://subdiff.org',
  permalinks: {
    trailingSlash: false
  },
  plugins: [
    {
      use: '@gridsome/source-filesystem',
      options: {
        path: 'src/data/blog/**/*.md',
        typeName: 'BlogPost',
        refs: {
          // creates GraphQL collection from tags in front-matter + adds reference
          tags: {
            typeName: 'BlogTag',
            create: true
          },
          authors: {
            typeName: 'Author'
          }
        }
      }
    },
    {
      use: '@gridsome/source-filesystem',
      options: {
        typeName: 'Project',
        baseDir: 'src/data',
        path: 'projects/*.md'
      }
    },
    {
      use: 'gridsome-plugin-feed',
      options: {
        contentTypes: ['BlogPost'],
        feedOptions: {
          title: 'subdiff.org blog',
          description: '',
          language: "en"
        },
        rss: {
          enabled: true,
          output: '/blog/feed.xml'
        },
        atom: {
          enabled: false,
          output: '/blog/feed.atom'
        },
        json: {
          enabled: false,
          output: '/blog/feed.json'
        },
        // Optional: the maximum number of items to include in your feed
        maxItems: 25,
        // Optional: an array of properties passed to `Feed.addItem()` that will be parsed for
        // URLs in HTML (ensures that URLs are full `http` URLs rather than site-relative).
        // To disable this functionality, set to `null`.
        htmlFields: ['description', 'content'],
        // Optional: a method that accepts a node and returns true (include) or false (exclude)
        // Example: only past-dated nodes: `filterNodes: (node) => node.fields.date <= new Date()`
        filterNodes: (node) => true,
        // Optional: a method that accepts a node and returns an object for `Feed.addItem()`
        // See https://www.npmjs.com/package/feed#example for available properties
        // NOTE: `date` field MUST be a Javascript `Date` object
        nodeToFeedItem: (node) => ({
          title: node.title,
          date: node.date,
          content: marked(node.content)
        })
      }
    }
  ],
  templates: {
    BlogPost: [
      {
        path: (node) => {
          year = `${node.date.getFullYear()}`;
          path = sluggify_to_url(`${node.title}`);
          return `/blog/${year}/${path}`
        }
      }
    ],
    BlogTag: [
      {
        path: (node) => {
          path = sluggify_to_url(`${node.id}`);
          return '/blog/tags/' + path;
        }
      }
    ]
  },
  transformers: {
    //Add markdown support to all file-system sources
    remark: {
      externalLinksTarget: '_blank',
      externalLinksRel: ['nofollow', 'noopener', 'noreferrer'],
      anchorClassName: 'icon icon-link',
      plugins: [
        '@gridsome/remark-prismjs',
        'gridsome-remark-figure-caption'
      ]
    }
  },
}
