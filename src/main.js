// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import VueDisqus from 'vue-disqus'
import VueScrollTo from 'vue-scrollto'
import DefaultLayout from '~/layouts/Default.vue'
import PostLayout from '~/layouts/PostLayout'

export default function (Vue, { router, head, isClient }) {
  Vue.use(VueDisqus)
  Vue.use(VueScrollTo)
  Vue.component('Layout', DefaultLayout)
  Vue.component('PostLayout', PostLayout)

  Vue.mixin({
    methods: {
      // img {url, alt, type, width, height}
      socialMeta: function (url, title, descr, img) {
        return [
          {
            property: 'og:url',
            content: url
          },
          {
            property: 'og:type',
            content: 'website'
          },
          {
            property: 'og:title',
            content: title
          },
          {
            property: 'og:description',
            content: descr
          },
          {
            property: 'og:image',
            content: img.url
          },
          {
            property: 'og:image:alt',
            content: img.alt
          },
          {
            property: 'og:image:type',
            content: img.type
          },
          {
            property: 'og:image:width',
            content: img.width
          },
          {
            property: 'og:image:height',
            content: img.height
          },
          {
            name: 'twitter:card',
            content: 'summary_large_image'
          },
          {
            name: 'twitter:image',
            content: img.url
          },
          {
            name: 'twitter:image:alt',
            content: img.alt
          },
          {
            name: 'twitter:description',
            content: descr
          }
        ]
      }
    }
  })

  Vue.directive('click-outside', {
    bind: function (el, binding, vnode) {
      el.clickOutsideEvent = function (event) {
        if (!(el == event.target || el.contains(event.target))) {
          vnode.context[binding.expression](event);
        }
      };
      document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function (el) {
      document.body.removeEventListener('click', el.clickOutsideEvent)
    },
  });
}
