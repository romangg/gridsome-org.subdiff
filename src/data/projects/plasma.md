---
name: KDE Plasma
logo: ./assets/plasma-logo.png
description: The KDE desktop shell and its utilities
color: "hsl(204, 86%, 53%)"
url: https://kde.org/plasma-desktop
tag: plasma
contributions:
- text: "New paradigm for display cloning (WIP)"
  url: "https://phabricator.kde.org/T11222"
- text: "KScreen Kirigami QML rewrite"
  url: "https://phabricator.kde.org/R104:6309d33f092e60a96ce38b32cb295a3e54347cb9"
- text: "Individual display property control"
  url: "https://phabricator.kde.org/T10028"
- text: Mouse control module for libinput on X
  url: https://phabricator.kde.org/D11469
- text: Mouse control module on Wayland
  url: https://phabricator.kde.org/D11468
- text: Night Color control module
  url: https://phabricator.kde.org/D5932
- text: Task-manager thumbnail redesign
  url: https://phabricator.kde.org/D3738
- text: Digital clock applet rework
  url: https://phabricator.kde.org/D3630
- text: Touchpad control module on Wayland
  url: https://phabricator.kde.org/D3617
---
