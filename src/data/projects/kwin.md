---
name: KWin
logo: ./assets/kwin.png
description: The KDE desktop window manager
color: "hsl(48, 100%, 67%)"
url: https://phabricator.kde.org/project/profile/98/
tag: kwin
contributions:
- text: Refactor Screens class (WIP)
  url: https://phabricator.kde.org/T11098
- text: Rework of compositing pipeline (WIP)
  url: https://phabricator.kde.org/T11071
- text: Drag and drop between XWayland and Wayland native windows
  url: https://phabricator.kde.org/T4611
- text: Usability of Pointer locks/confinment on Wayland
  url: https://phabricator.kde.org/T8923
- text: Abstracting internal Output representation
  url: https://phabricator.kde.org/D11781
- text: Night Color - KWin's native red shift utility
  url: https://phabricator.kde.org/D5928
- text: DRM backend rework
  url: https://phabricator.kde.org/D5118
- text: libinput device config write and save
  url: https://phabricator.kde.org/D3460
- text: Initial Atomic Mode Setting implementation
  url: https://phabricator.kde.org/D2370
---
