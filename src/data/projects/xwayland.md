---
name: XWayland
logo: ./assets/xwayland-logo.png
description: Legacy support for X clients on Wayland
color: "hsl(171, 100%, 41%)"
url: https://wayland.freedesktop.org/xserver.html
tag: xwayland
contributions:
- text: Multi DPI (WIP)
  url: https://gitlab.freedesktop.org/xorg/xserver/merge_requests/111
- text: Present support
  url: https://patchwork.freedesktop.org/series/39139/
---
