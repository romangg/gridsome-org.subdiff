---
name: Wayland
logo: ./assets/wayland-logo.png
description: The Unix windowing system of the future
color: "hsl(141, 71%, 48%)"
url: https://wayland.freedesktop.org/
tag: wayland
contributions:
- text: xdg-output support in Weston
  url: https://gitlab.freedesktop.org/wayland/weston/merge_requests/143
- text: Session suspension and restoration protocol (WIP)
  url: https://lists.freedesktop.org/archives/wayland-devel/2018-June/038540.html
---
